# Final Project Assignment for ICL 16/17

This assignment is the final project for the edition of ICL 16/17 course. 

## Introduction

You mission is to develop an interpreter, a type system, and a compiler for an object-oriented imperative language. In this document you may find the syntax and explanation of most expressions.  

## Syntax

~~~
exp ::= 
	| num
	| true 
	| false
	| exp '+' exp | exp '-' exp | exp '*' exp | exp '+' exp | '-' exp | '(' exp ')' 
	| exp '<' exp | exp '>' exp | exp '==' exp | exp '!=' exp | ...
	| exp '&&' exp | exp '||' exp | '!' exp 
	| id
	| 'decl' bindings 'in' exp 'end' 
    | 'var' '(' exp ')' 
    | '*' exp
    | exp ':=' exp 
    | exp ';' exp 
    | 'while' exp 'do' exp 'end' 
    | 'if' exp 'then' exp 'else' exp 'end' 
    | 'fun' typemap '=>' exp 'end' 
    | 'exp' '(' arguments ')'
	| '{' fields '}'
	| exp '.' label
	| 'declrec' typedbindings 'in' exp 'end' 
	| '{{' fields '}}'
	| 'this'

num ::= '0' | ['1'-'9']\['0'-'9'\]*
    
bindings ::= id '=' exp | id '=' exp bindings 

typedbindings ::= id ':' type '=' exp | id ':' type '=' exp typedbindings 

type ::= 'int' | 'bool' | 'ref' '(' type ')' | '(' types ')' '->' type | '{{' typemap '}}' | SelfType

typemap ::= <empty> | typemaplist
typemaplist ::= id ':' type | id ':' type ',' typemaplist

types ::= <empty> | typelist
typelist ::= type | type ',' typelist

arguments ::= <empty> | arglist
arglist ::= exp | exp ',' arglist

fields ::= <empty> | fieldlist
fieldlist ::= id '=' exp | id '=' exp ',' fieldlist
~~~

The language constructs expected in the final project are the ones introduced by the lab assignments
and then extended with records, recursive declarations, and objects. The language includes integer literals  (`num`), boolean literals (`true` and `false`), and all expressions around integer and boolean values. The language also includes identifiers and the corresponding declaration expressions. The language is imperative with free references, and thus include the creation of state variables (`var(exp)`), dereferencing (`*exp`), and assignment (`exp := exp`), sequencing of effectful expressions, while loops and decision expressions. Besides imperative constructs, the language includes functions as first-class values (closures), introduced by function introduction expression (`fun parameters => exp end`) and function call expression (`exp (exp)`). 

The last set of expressions in the language are records, defined by a set of field definitions and corresponding selection expression (`exp.label`). A recursive declaration (`declrec`) and records are the basis to define objects, with the special identifier `this`. Identifiers (`id`) and record labels (`label`) are defined in the standard ways.

# Semantics

The operational semantics of most expressions is standard and mostly explained in the lecture slides. Objects are mutually recursive definition of fields, and `this` is a reserved keyword that denotes the whole object. The semantics of objects should be implemented by an encoding to the base language by a recursive definition of the this identifier.

# Type semantics
 
The type semantics of the language is standard and mostly explained in the lecture slides. The typing of objects is also recursive and uses the special type `SelfType` to denote the type of the object itself. The typing of object expressions must be performed from scratch and is not possible to obtain by an encoding. 

# Project submission and discussion

The project source and tests is submitted during a discussion session between 20 and 22 of December 2016. The schedule will be available at the department secretariat office.  

# Evaluation 

Project evaluation follows a partition given to interpreter (8 points / 20 points), type system (4 points / 20 points), and compiler (8 points / 20 points). The correct and complete implementation of the recursive definition of objects gives a bonus of 2 extra points in the final score, to a maximum of 20 points. The thourough use of unit tests gives a bonus of 1 extra point in the final score, to a maximum of 20 points. 



