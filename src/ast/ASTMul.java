package ast;

import compiler.CodeBlock;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;

public class ASTMul implements ASTNode {

	ASTNode left, right;

	public ASTMul(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		return new IntValue(((IntValue)left.eval(env)).getValue() *
				((IntValue)right.eval(env)).getValue());
	}

	@Override
	public void compile(CodeBlock code) {
		this.left.compile(code);
		this.right.compile(code);
		code.emit_mul();		
	}

	@Override
	public String toString() {
		return left.toString() + " * " + right.toString();
	}
}
